<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $havePreferences = PreferencesController::havePreferences();
        $recomendations = $havePreferences ? RecomendationsController::get() : array();
        return view('home')->with('havePreferences', $havePreferences )->with('recomendations', $recomendations);
    }
}
