<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use PhpParser\Builder\Class_;

class RecomendationsController extends Controller
{
    private static function recomendationsQuery() {
//        return DB::table('residences')->join('users', 'users.id', '=', 'residences.user_id')->select(
//            'residences.country as country',
//            'residences.city as city',
//            'residences.region as region',
//            'residences.additional as additional',
//            'users.name as user_name',
//            'users.email as user_email',
//            'users.phone_number as user_phone_number'
//        )->where('residences.user_id', '<>', Auth::user()->id)->whereExists(function ($query) {
//            $query->select(DB::raw(1))->from('preferences')->where('preferences.user_id', '=', Auth::user()->id)->where(function ($query) {
//                $query->where('preferences.country', '=', 'residences.country');
//            })->where(function ($query) {
//                $query->where('preferences.city', '=', 'residences.city')->orWhere('preferences.city', '=', '');
//            })->where(function ($query) {
//                $query->where('preferences.region', '=', 'residences.region')->orWhere('preferences.region', '=', '');
//            })/*->where(function ($query) {
//                $query->where('residences.additional', 'like', '%'.'preferences.additional'.'%');
//            })*/;
//        });
        return 'select `residences`.`country` as `country`, `residences`.`city` as `city`, `residences`.`region` as `region`, `residences`.`additional` as `additional`, `users`.`name` as `user_name`, `users`.`email` as `user_email`, `users`.`phone_number` as `user_phone_number` from `residences` inner join `users` on `users`.`id` = `residences`.`user_id` where `residences`.`user_id` <> ' . Auth::user()->id .' and exists (select 1 from `preferences` where `preferences`.`user_id` = ' . Auth::user()->id .' and (`preferences`.`country` = `residences`.`country`) and (`preferences`.`city` = `residences`.`city` or `preferences`.`city` = "") and (`preferences`.`region` = `residences`.`region` or `preferences`.`region` = "") and `residences`.`additional` like concat("%", `preferences`.`additional` , "%"))';
    }

    public static function get() {
        //DB::enableQueryLog();
        //return RecomendationsController::recomendationsQuery()->limit(5)->get();
        return DB::select(self::recomendationsQuery() . ' limit 5');
    }

    public function more(Request $request) {
        try {
            $this->validate($request, [
                'offset' => 'required|numeric'
            ]);
        } catch (ValidationException $e) {
            return response()->json(null, 403);
        }

        //$results = $this->recomendationsQuery()->offset($request->get('offset'))->limit(5)->get();
        $results = DB::select(self::recomendationsQuery() . ' offset ' . $request->get('offset') . ' limit 5');

        return response()->json([
            'success' => true,
            'results' => $results
        ]);
    }


}
