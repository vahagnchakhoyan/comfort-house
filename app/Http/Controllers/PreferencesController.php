<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class PreferencesController extends Controller
{
    public static function havePreferences() {
        return DB::table('preferences')->where('user_id', '=', Auth::user()->id)->first() != null;
    }

    public function get(Request $request) {
        $userId = Auth::user()->id;
        $preferences = DB::table('preferences')->where('user_id', '=', $userId)->get();
        return view('myPreferences')->with('preferences', $preferences);
    }

    public function register(Request $request) {
        try {
            $this->validate($request, [
                'country' => 'required|exists:countries,name',
                'residenceArea' => 'required|min:1|max:150',
                'region' => 'nullable|max:100',
                'additional' => 'nullable|max:100',
            ]);
        } catch (ValidationException $e) {
            return response()->json(null, 403);
        }

        $newId = DB::table('preferences')->insertGetId([
            'user_id' => Auth::user()->id,
            'country' => $request->get('country'),
            'city' => $request->get('residenceArea'),
            'region' => $request->get('region') != null ? $request->get('region') : "",
            'additional' => $request->get('additional') != null ? $request->get('additional') : "",
        ]);

        return redirect('/my_preferences');
    }

    public function remove(Request $request) {
        try {
            $this->validate($request, [
                'preferenceId' => 'required|numeric'
            ]);
        } catch (ValidationException $e) {
            return response()->json(null, 403);
        }

        if(!$this->belongsToUser($request->get('preferenceId'))){
            return response()->json(null, 403);
        }

        DB::table('preferences')->where('id', '=', $request->get('preferenceId'))->delete();

        return response()->json([
            'success' => true
        ]);
    }

    protected function belongsToUser(string $preferenceId) {
        $preference = DB::table('preferences')->where('id', '=', $preferenceId)->first();
        return $preference != null && $preference->user_id == Auth::user()->id;
    }
}
