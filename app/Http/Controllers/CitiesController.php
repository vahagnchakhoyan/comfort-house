<?php

namespace App\Http\Controllers;

use Dotenv\Exception\ValidationException;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CitiesController extends Controller
{
    public function getCities(Request $request) {
        try {
            $this->validate($request, [
                'prefix' => 'required|string|min:1'
            ]);
        } catch(ValidationException $e) {
            return response()->json(null, 403);
        }

        $result = DB::table('cities')->select('full_name_nd')->where('full_name_nd', 'like', $request->get('prefix') . '%')->take(5)->get();

        return response()->json($result);
    }
}
