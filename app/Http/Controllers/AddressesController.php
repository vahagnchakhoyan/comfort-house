<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class AddressesController extends Controller
{
    public function get(Request $request) {
        $userId = Auth::user()->id;
        $addresses = DB::table('residences')->where('user_id', '=', $userId)->get();
        $default = false;
        $addresses = $addresses->reject(function ($value, $key) use (&$default) {
            if($value->id == Auth::user()->default_residence){
                $default = $value;
                return true;
            }
            return false;
        });
        return view('myAddresses')->with('addresses', $addresses)->with('default', $default);
    }

    public function register(Request $request) {
        try {
            $this->validate($request, [
                'country' => 'required|exists:countries,name',
                'residenceArea' => 'required|min:1|max:150',
                'region' => 'nullable|max:100',
                'additional' => 'nullable|max:100',
            ]);
        } catch (ValidationException $e) {
            return response()->json(null, 403);
        }

        $newId = DB::table('residences')->insertGetId([
            'user_id' => Auth::user()->id,
            'country' => $request->get('country'),
            'city' => $request->get('residenceArea'),
            'region' => $request->get('region') != null ? $request->get('region') : "",
            'additional' => $request->get('additional') != null ? $request->get('additional') : "",
        ]);

        return redirect('/my_addresses');
    }

    public function remove(Request $request) {
        try {
            $this->validate($request, [
                'addressId' => 'required|numeric'
            ]);
        } catch (ValidationException $e) {
            return response()->json(null, 403);
        }

        if(!$this->belongsToUser($request->get('addressId')) || Auth::user()->default_residence == $request->get('addressId')){
            return response()->json(null, 403);
        }

        DB::table('residences')->where('id', '=', $request->get('addressId'))->delete();

        return response()->json([
            'success' => true
        ]);
    }

    protected function belongsToUser(string $addressId) {
        $address = DB::table('residences')->where('id', '=', $addressId)->first();
        return $address != null && $address->user_id == Auth::user()->id;
    }
}
