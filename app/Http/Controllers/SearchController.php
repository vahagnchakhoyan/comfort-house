<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class SearchController extends Controller
{
    public function get(Request $request) {
        return view('search');
    }

    public function search(Request $request) {
        try {
            $this->validate($request, [
                'country' => 'required|exists:countries,name',
                'residenceArea' => 'nullable|max:150',
                'region' => 'nullable|max:100',
                'additional' => 'nullable|max:100',
            ]);
        } catch (ValidationException $e) {
            return response()->json(null, 403);
        }

        $conditions = array();
        array_push($conditions, ['country', '=', $request->get('country')]);

        if($request->filled('residenceArea')) array_push($conditions, ['city', '=', $request->get('residenceArea')]);
        if($request->filled('region')) array_push($conditions, ['region', '=', $request->get('region')]);
        if($request->filled('additional')) array_push($conditions, ['additional', 'like', '%'.$request->get('additional').'%']);
        array_push($conditions, ['user_id', '<>', Auth::user()->id]);

        $results = DB::table('residences')->join('users', 'users.id', '=', 'residences.user_id')->select(
            'residences.country as country',
            'residences.city as city',
            'residences.region as region',
            'residences.additional as additional',
            'users.name as user_name',
            'users.email as user_email',
            'users.phone_number as user_phone_number'
        )->where($conditions)->limit(5)->get();

        return response()->json([
            'success' => true,
            'results' => $results
        ]);
    }

    public function more(Request $request) {
        try {
            $this->validate($request, [
                'country' => 'required|exists:countries,name',
                'residenceArea' => 'nullable|max:150',
                'region' => 'nullable|max:100',
                'additional' => 'nullable|max:100',
                'offset' => 'required|numeric'
            ]);
        } catch (ValidationException $e) {
            return response()->json(null, 403);
        }
        $conditions = array();
        array_push($conditions, ['country', '=', $request->get('country')]);

        if($request->filled('residenceArea')) array_push($conditions, ['city', '=', $request->get('residenceArea')]);
        if($request->filled('region')) array_push($conditions, ['region', '=', $request->get('region')]);
        if($request->filled('additional')) array_push($conditions, ['additional', 'like', '%'.$request->get('additional').'%']);
        array_push($conditions, ['user_id', '<>', Auth::user()->id]);

        $results = DB::table('residences')->join('users', 'users.id', '=', 'residences.user_id')->select(
            'residences.country as country',
            'residences.city as city',
            'residences.region as region',
            'residences.additional as additional',
            'users.name as user_name',
            'users.email as user_email',
            'users.phone_number as user_phone_number'
        )->where($conditions)->offset($request->get('offset'))->limit(5)->get();

        return response()->json([
            'success' => true,
            'results' => $results
        ]);
    }
}
