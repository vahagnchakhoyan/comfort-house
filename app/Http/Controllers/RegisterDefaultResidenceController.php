<?php

namespace App\Http\Controllers;

use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RegisterDefaultResidenceController extends Controller
{
    public function getView(Request $request) {
        return view("registerDefaultResidence");
    }

    public function register(Request $request) {
        try {
            $this->validate($request, [
                'country' => 'required|exists:countries,name',
                'residenceArea' => 'required|min:1|max:150',
                'region' => 'nullable|max:100',
                'additional' => 'nullable|max:100',
            ]);
        } catch (ValidationException $e) {
            return response()->json(null, 403);
        }
        $newId = DB::table('residences')->insertGetId([
            'user_id' => Auth::user()->id,
            'country' => $request->get('country'),
            'city' => $request->get('residenceArea'),
            'region' => $request->get('region') != null ? $request->get('region') : "",
            'additional' => $request->get('additional') != null ? $request->get('additional') : "",
        ]);

        DB::table('users')->where('id', '=', Auth::user()->id)->update(['default_residence' => $newId]);

        return redirect('/home');
    }

    public function set(Request $request) {
        try {
            $this->validate($request, [
                'addressId' => 'required|numeric'
            ]);
        } catch (ValidationException $e) {
            return response()->json(null, 403);
        }

        if(!$this->belongsToUser($request->get('addressId')) || Auth::user()->default_residence == $request->get('addressId')){
            return response()->json(null, 403);
        }

        DB::table('users')->where('id', '=', Auth::user()->id)->update(['default_residence' => $request->get('addressId')]);

        return response()->json([
            'success' => true
        ]);
    }

    protected function belongsToUser(string $addressId) {
        $address = DB::table('residences')->where('id', '=', $addressId)->first();
        return $address != null && $address->user_id == Auth::user()->id;
    }
}
