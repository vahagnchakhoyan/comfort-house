<?php

namespace App\Http\Middleware;

use Closure;

class CheckDefaultResidence
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if($user->default_residence == null) {
            return redirect('/register_default_address');
        }

        return $next($request);
    }
}
