<?php

namespace App\Http\Middleware;

use Closure;

class HaveNoDefaultResidence
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if($user->default_residence != null) {
            return redirect('home');
        }

        return $next($request);
    }
}
