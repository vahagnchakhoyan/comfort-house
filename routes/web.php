<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware('haveDefaultResidence');
Route::get('/thanks', function () { return view("thanks"); });

Route::get('/register_default_address', 'RegisterDefaultResidenceController@getView')->middleware('auth', 'haveNoDefaultResidence');
Route::post('/register_default_address', 'RegisterDefaultResidenceController@register')->middleware('auth', 'haveNoDefaultResidence');

Route::post('/register_preference', 'PreferencesController@register')->middleware('auth', 'haveDefaultResidence');
Route::post('/register_address', 'AddressesController@register')->middleware('auth', 'haveDefaultResidence');

Route::post('/get_cities', 'CitiesController@getCities')->middleware('auth');

Route::get('/enter', 'ListController@enter');

Route::get('/search', 'SearchController@get')->middleware('auth', 'haveDefaultResidence');
Route::post('/search', 'SearchController@search')->middleware('auth', 'haveDefaultResidence');
Route::post('/more_search_results', 'SearchController@more')->middleware('auth', 'haveDefaultResidence');

Route::post('/more_recomendation_results', 'RecomendationsController@more')->middleware('auth', 'haveDefaultResidence');

Route::get('/my_addresses', 'AddressesController@get')->middleware('auth', 'haveDefaultResidence');
Route::get('/my_preferences', 'PreferencesController@get')->middleware('auth', 'haveDefaultResidence');

Route::post('/remove_preference', 'PreferencesController@remove')->middleware('auth', 'haveDefaultResidence');
Route::post('/remove_address', 'AddressesController@remove')->middleware('auth', 'haveDefaultResidence');

Route::post('/set_default_residence', 'RegisterDefaultResidenceController@set')->middleware('auth', 'haveDefaultResidence');
