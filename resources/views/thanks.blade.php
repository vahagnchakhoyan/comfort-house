@extends('layouts.app')
<style>
    @font-face {
        font-family: BiteChocolate;
        src: url(/fonts/bite_chocolate.ttf);
    }
</style>

@section('content')
    <div class="shadow-sm bg-white content" style="margin-left: 10px; margin-right: 10px; font-family: BiteChocolate">
        <p style="color: #000fbb; padding-top: 10px; padding-left: 10px; padding-right: 10px; padding-bottom: 10px">
            Special thanks to me.<br>
            To my father for great idea.<br>
            To GeoDataSource for cities.<br>
            To online photoshop.<br>
            To google search.<br>
            And to me again.
        </p>
    </div>
@endsection