@extends('layouts.framed')

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        autocomplete(document.getElementById('ResidenceArea'), citiesGetter, 1000);
    }, false);

    function search() {
        document.getElementById("SearchButton").disabled = true;
        document.getElementById("MoreButton").disabled = true;
        $.post('/search', {country: document.getElementById("CountriesSelect").value,
            residenceArea: document.getElementById("ResidenceArea").value,
            region: document.getElementById("Region").value,
            additional: document.getElementById("Additional").value
    }, function(data) {
            document.getElementById("SearchButton").disabled = false;
            document.getElementById("MoreButton").disabled = false;
            if(data.success) {
                document.getElementById("Results").style.display = "block";
                document.getElementById("More").style.display = data.results.length == 5 ? "block" : "none";
                document.searchOffset = data.results.length;

                let resultsBody = document.getElementById("ResultsBody");
                while (resultsBody.firstChild) {
                    resultsBody.removeChild(resultsBody.firstChild);
                }

                if(data.results.length == 0) {
                    resultsBody.innerHTML += "No search results found.";
                    return;
                }

                data.results.forEach(function (result, index) {
                    let newDiv = document.createElement("div");
                    newDiv.className = "shadow-glass nunitoFonted";
                    newDiv.style.backgroundColor = "#e5882e";
                    insertInfoIntoDiv(newDiv, result)
                    resultsBody.appendChild(newDiv);
                    if(index != data.results.length - 1) resultsBody.appendChild(document.createElement("br"));
                });
            }
        });
    }

    function insertInfoIntoDiv(div, source) {
        div.innerHTML += "COUNTRY: " + source.country + "<br>";
        div.innerHTML += "CITY, VILLAGE OR OTHER: " + source.city + "<br>";
        div.innerHTML += "REGION: " + source.region + "<br>";
        div.innerHTML += "DETAILED: " + source.additional + "<br>";
        div.innerHTML += "<br>";
        div.innerHTML += "CONTACT INFO <br>";
        div.innerHTML += "NAME: " + source.user_name + "<br>";
        div.innerHTML += "EMAIL: " + source.user_email + "<br>";
        div.innerHTML += "PHONE: " + source.user_phone_number;
    }

    function moreResults() {
        document.getElementById("SearchButton").disabled = true;
        document.getElementById("MoreButton").disabled = true;
        $.post('/more_search_results', {country: document.getElementById("CountriesSelect").value,
            residenceArea: document.getElementById("ResidenceArea").value,
            region: document.getElementById("Region").value,
            additional: document.getElementById("Additional").value,
            offset: document.searchOffset
        }, function(data) {
            document.getElementById("SearchButton").disabled = false;
            document.getElementById("MoreButton").disabled = false;
            if(data.success) {
                document.getElementById("More").style.display = data.results.length == 5 ? "block" : "none";
                document.searchOffset += data.results.length;

                let resultsBody = document.getElementById("ResultsBody");

                data.results.forEach(function (result, index) {
                    if(index == 0) resultsBody.appendChild(document.createElement("br"));
                    let newDiv = document.createElement("div");
                    newDiv.className = "shadow-glass nunitoFonted";
                    newDiv.style.backgroundColor = "#e5882e";
                    insertInfoIntoDiv(newDiv, result)
                    resultsBody.appendChild(newDiv);
                    if(index != data.results.length - 1) resultsBody.appendChild(document.createElement("br"));
                });
            }
        });
    }
</script>

@section('main_content')
    <div class="card">
        <div class="card-header summerBehind">{{ __('Search') }}</div>

        <div class="card-body">
            @include('bits.residenceFormBody')

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button id="SearchButton" type="submit" class="btn btn-primary" onclick="search()">
                        {{ __('Search') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div id="Results" class="card" style="display: none">
        <div class="card-header summerBehind">{{ __('Search Results') }}</div>

        <div id="ResultsBody" class="card-body">

        </div>
        <div id="More" class="card-body" style="display: none">
            <button id="MoreButton" class="transparent-button" style="position: absolute; margin-top: 5px; height: 30px; width: 30px" onclick="moreResults()">
                <img src="/images/plus_orange_icon.png" style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%;">
            </button>
        </div>
    </div>
@endsection