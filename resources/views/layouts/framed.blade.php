@extends('layouts.app')
<style>
    .wrapper {
        display: flex;
        align-items: stretch;
    }

    #sidebar {
        min-width: 200px;
        max-width: 250px;
        min-height: 100vh;
    }

    #sidebar.active {
        margin-left: -200px;
    }

    a[data-toggle="collapse"] {
        position: relative;
    }

    @media (max-width: 768px) {
        #sidebar {
            margin-left: -200px;
        }
        #sidebar.active {
            margin-left: 0;
        }
    }
</style>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>

@section('content')
    <div class="wrapper">
         <nav id="sidebar" style="padding-left: 8px; padding-right: 8px; border: 2px solid #4488ff; border-radius: 4px; background-image: url('/images/winter-background.jpg'); background-size: cover">
             <div class="sidebar-header">
                 <h4 class="biteChocolate" style="margin-bottom: 0px; margin-top: 2px">menu</h4>
             </div>
             <hr style="margin-top: 8px">

             <ul class="list-unstyled components">
                 <li>
                     <a href="/home">Home</a>
                 </li>
                 <li>
                     <a href="/search">Search</a>
                 </li>
                 <li>
                     <a href="/my_addresses">My Addresses</a>
                 </li>
                 <li>
                     <a href="/my_preferences">My Preferences</a>
                 </li>
                 <li>
                     <a href="/thanks">Special Thanks</a>
                 </li>
             </ul>
        </nav>

        <div id="content" style="position: relative; left: 0px; right: 0px; width: 100%">
            <button type="button" id="sidebarCollapse" class="transparent-button">
                <img src="/images/left_right_icon.png" style="height: 20px; width: 20px">
            </button>
            <div style="position: absolute; left: 32px; top: 0px; right: 0px; height: 100%">
                @yield('main_content')
            </div>
        </div>
    </div>
@endsection
