@extends('layouts.framed')

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        autocomplete(document.getElementById('ResidenceArea'), citiesGetter, 1000);
    }, false);

    function showForm() {
        document.getElementById("AddressForm").style.display = "block";
    }

    function hideForm() {
        document.getElementById("AddressForm").style.display = "none";
    }

    function removeAddress(addressId) {
        if(!confirm('Are you sure want to remove address?')) {
            return;
        }

        $.post('/remove_address', {addressId: addressId}, function(data) {
            if(data.success) {
                location.reload();
            }
        });
    }

    function setDefault(addressId) {
        if(!confirm('Are you sure want to set as default?')) {
            return;
        }

        $.post('/set_default_residence', {addressId: addressId}, function(data) {
            if(data.success) {
                location.reload();
            }
        });
    }
</script>

@section('main_content')
    <div id="AddressForm" class="overlay" style="overflow: scroll">
        <div class="card" style="horiz-align: center; margin: 0 auto; width: 75%; min-width: 300px">
            <div class="card-header" style="position: relative; width: 100%">{{ __('Register Address') }}
                <button class="transparent-button x_button">
                    <img class="equalParent" src="/images/x_icon.png" onclick="hideForm()">
                </button>
            </div>

            <div class="card-body">
                <form method="POST" action="register_address">
                    @csrf

                    @include('bits.residenceFormBody')

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="card">
        <dir class="card-header summerBehind" style="margin-top: 0px">
            Default
        </dir>
        <div class="card-body">
            <div class="shadow-glass nunitoFonted" style="position: relative; width: 100%; background-color: #e5882e">
                COUNTRY: {{$default->country}}<br>
                CITY, VILLAGE OR OTHER: {{$default->city}}<br>
                REGION: {{$default->region}}<br>
                DETAILED: {{$default->additional}}
            </div>
            <br>
        </div>
    </div>
    <br>

    <div class="card">
        <dir class="card-header summerBehind" style="margin-top: 0px">
            Addresses List
        </dir>
        <div class="card-body">
            @if(count($addresses) == 0)
                Your addresses list is empty.<br>
                You can add several.
            @endif

            @foreach($addresses as $address)
                <div class="shadow-glass nunitoFonted" style="position: relative; width: 100%; background-color: #e5882e">
                    COUNTRY: {{$address->country}}<br>
                    CITY, VILLAGE OR OTHER: {{$address->city}}<br>
                    REGION: {{$address->region}}<br>
                    DETAILED: {{$address->additional}}
                    <button title="Remove" class="transparent-button" onclick="removeAddress({{$address->id}})" style="position: absolute; right: 4px; top: 4px; height: 20px; width: 20px">
                        <img class="equalParent" src="/images/orange_trash_icon.png">
                    </button>
                    <button title="Set Default" class="transparent-button" onclick="setDefault({{$address->id}})" style="position: absolute; right: 4px; top: 32px; height: 20px; width: 20px">
                        <img class="equalParent" src="/images/green_home_icon.png">
                    </button>
                </div>
                <br>
            @endforeach
        </div>
        <div class="card-body">
            <button class="transparent-button" style="position: absolute; height: 35px; width: 35px" onclick="showForm()">
                <img src="/images/add_house_icon.png" style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%;">
            </button>
        </div>
    </div>
@endsection