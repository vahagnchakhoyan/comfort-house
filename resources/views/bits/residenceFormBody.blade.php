<div class="form-group row">
    <label for="CountriesSelect" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

    <div class="col-md-6">
        @include('bits.countriesList')

        @error('country')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="ResidenceArea" class="col-md-4 col-form-label text-md-right">City, village or other</label>

    <div class="col-md-6">
        <input id="ResidenceArea" type="text" class="form-control @error('residenceArea') is-invalid @enderror" name="residenceArea" value="{{ old('residenceArea') }}" required autocomplete="residenceArea" placeholder="Area name" maxlength="150">

        @error('residenceArea')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="Region" class="col-md-4 col-form-label text-md-right">Region</label>

    <div class="col-md-6">
        <input id="Region" type="text" class="form-control @error('region') is-invalid @enderror" name="region" value="{{ old('region') }}" placeholder="Region in area, not required" maxlength="100">

        @error('region')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="Additional" class="col-md-4 col-form-label text-md-right">{{ __('Detailed') }}</label>

    <div class="col-md-6">
        <input id="Additional" type="text" class="form-control @error('additional') is-invalid @enderror" name="additional" autocomplete="additional" placeholder="Street, building ... not required" maxlength="100">

        @error('additional')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
    </div>
</div>
