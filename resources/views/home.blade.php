@extends('layouts.framed')

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        document.recomendationsOffset = {{count($recomendations)}};
    }, false);

    function insertInfoIntoDiv(div, source) {
        div.innerHTML += "COUNTRY: " + source.country + "<br>";
        div.innerHTML += "CITY, VILLAGE OR OTHER: " + source.city + "<br>";
        div.innerHTML += "REGION: " + source.region + "<br>";
        div.innerHTML += "DETAILED: " + source.additional + "<br>";
        div.innerHTML += "<br>";
        div.innerHTML += "CONTACT INFO <br>";
        div.innerHTML += "NAME: " + source.user_name + "<br>";
        div.innerHTML += "EMAIL: " + source.user_email + "<br>";
        div.innerHTML += "PHONE: " + source.user_phone_number;
    }

    function moreResults() {
        document.getElementById("MoreButton").disabled = true;
        $.post('/more_recomendation_results', {
            offset: document.recomendationsOffset
        }, function(data) {
            document.getElementById("MoreButton").disabled = false;
            if(data.success) {
                document.getElementById("More").style.display = data.results.length == 5 ? "block" : "none";
                document.recomendationsOffset += data.results.length;

                let resultsBody = document.getElementById("Recomendations");

                data.results.forEach(function (result, index) {
                    if(index == 0) resultsBody.appendChild(document.createElement("br"));
                    let newDiv = document.createElement("div");
                    newDiv.className = "shadow-glass nunitoFonted";
                    newDiv.style.backgroundColor = "#e5882e";
                    insertInfoIntoDiv(newDiv, result)
                    resultsBody.appendChild(newDiv);
                    if(index != data.results.length - 1) resultsBody.appendChild(document.createElement("br"));
                });
            }
        });
    }
</script>

@section('main_content')
    <div class="card">
        <div class="card-header summerBehind">{{ __('My Account') }}</div>

        <div class="card-body" style="text-align: center">
            NAME: {{\Illuminate\Support\Facades\Auth::user()->name}}<br>
            EMAIL: {{\Illuminate\Support\Facades\Auth::user()->email}}<br>
            PHONE: {{\Illuminate\Support\Facades\Auth::user()->phone_number}}<br>
        </div>
    </div>
    <br>
    <div class="card">
        <div class="card-header summerBehind">{{ __('Recomendations') }}</div>

        <div id="Recomendations" class="card-body">
            @if(!$havePreferences)
                You haven't registered any preference.<br>
                So we can't give you recomendations.<br>
                If you want to register any preference click the following link<br>
                <a href="/my_preferences">My Preferences</a>
            @elseif(count($recomendations) == 0)
                We haven't found any recomendation for you.<br>
            @else
                @foreach($recomendations as $recomendation)
                    <div class="shadow-glass nunitoFonted" style="position: relative; width: 100%; background-color: #e5882e">
                        COUNTRY: {{$recomendation->country}}<br>
                        CITY, VILLAGE OR OTHER: {{$recomendation->city}}<br>
                        REGION: {{$recomendation->region}}<br>
                        DETAILED: {{$recomendation->additional}}<br>
                        <br>
                        CONTACT INFO <br>
                        NAME: {{$recomendation->user_name}}<br>
                        EMAIL: {{$recomendation->user_email}}<br>
                        PHONE: {{$recomendation->user_phone_number}}
                    </div>
                    @if(!$loop->last)
                        <br>
                    @endif
                @endforeach
            @endif
        </div>
        <div id="More" class="card-body" style="display: @if(count($recomendations) < 5) none @else block @endif">
            <button id="MoreButton" class="transparent-button" style="position: absolute; margin-top: 5px; height: 30px; width: 30px" onclick="moreResults()">
                <img src="/images/plus_orange_icon.png" style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%;">
            </button>
        </div>
    </div>
@endsection