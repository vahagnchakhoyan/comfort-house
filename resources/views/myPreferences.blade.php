@extends('layouts.framed')

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        autocomplete(document.getElementById('ResidenceArea'), citiesGetter, 1000);
    }, false);
    
    function showForm() {
        document.getElementById("PreferenceForm").style.display = "block";
    }

    function hideForm() {
        document.getElementById("PreferenceForm").style.display = "none";
    }

    function removePreference(preferenceId) {
        if(!confirm('Are you sure want to remove preference?')) {
            return;
        }

        $.post('/remove_preference', {preferenceId: preferenceId}, function(data) {
            if(data.success) {
                location.reload();
            }
        });
    }
</script>

@section('main_content')
    <div id="PreferenceForm" class="overlay" style="overflow: scroll">
        <div class="card" style="horiz-align: center; margin: 0 auto; width: 75%; min-width: 300px">
            <div class="card-header" style="position: relative; width: 100%">{{ __('Register Preference') }}
                <button class="transparent-button x_button">
                    <img class="equalParent" src="/images/x_icon.png" onclick="hideForm()">
                </button>
            </div>

            <div class="card-body">
                <form method="POST" action="register_preference">
                    @csrf

                    @include('bits.residenceFormBody')

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="card">
        <dir class="card-header summerBehind" style="margin-top: 0px">
            Preferences list
        </dir>
        <div class="card-body">
            @if(count($preferences) == 0)
                Your preferences list is empty.<br>
                You can add several.
            @endif

            @foreach($preferences as $preference)
                <div class="shadow-glass nunitoFonted" style="position: relative; width: 100%; background-color: #e5882e">
                    COUNTRY: {{$preference->country}}<br>
                    CITY, VILLAGE OR OTHER: {{$preference->city}}<br>
                    REGION: {{$preference->region}}<br>
                    DETAILED: {{$preference->additional}}
                    <button title="Remove" class="transparent-button" onclick="removePreference({{$preference->id}})" style="position: absolute; right: 4px; top: 4px; height: 20px; width: 20px">
                        <img class="equalParent" src="/images/orange_trash_icon.png">
                    </button>
                </div>
                <br>
            @endforeach
        </div>
        <div class="card-body">
            <button class="transparent-button" style="position: absolute; height: 35px; width: 35px" onclick="showForm()">
                <img src="/images/add_house_icon.png" style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%;">
            </button>
        </div>
    </div>
@endsection